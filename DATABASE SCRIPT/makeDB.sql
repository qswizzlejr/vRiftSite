create database vuser;

use vuser;

create table phone(
	phoneid int primary key auto_increment,
	number char(10),
	phonetype char(4));

create table member(
	memberid int primary key auto_increment,
	fname varchar(15),
	lname varchar(15),
	uname varchar(15),
	pass	varchar(30),
	email varchar(30),
	birthdate date,
	signupdate timestamp default current_timestamp(),
	phoneid int,
	foreign key (phoneid) references phone(phoneid));