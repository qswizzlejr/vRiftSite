﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace vrift.WebPages.Content
{
    public partial class Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string connStr = "server=localhost;user=root;database=vuser;port=3306;password='';SslMode=none;";
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {

                //I'm doing this for testing purposes
                conn.Open();

                //write our SQL statement as a string
                string sql = "select fname, lname, uname, pass, email, birthdate, signupdate, number from member m join phone p on m.phoneid = p.phoneid where uname = @un;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                //add values to our placeholders
                cmd.Parameters.AddWithValue("@un", Session["acct"]);

                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                grid.DataSource = ds;
                grid.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }

            //close our database connection
            conn.Close();
        }
    }
}
