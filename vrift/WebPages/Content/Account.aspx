﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebPages/Master/vrift.Master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="vrift.WebPages.Content.Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
        <div class="imgholder">
        <img class="headerpic" width="1920" height="1080" src="../../Content/img/random/1.png" />
    </div>
    <div class="picturesbottom"></div>
    <div class="blocker">
        <div class="mainstuff">
            <hr />
            <h2>Account Details</h2>
            <hr />
            <form runat="server">
                <asp:GridView ID="grid" runat="server" BorderColor="Gray" BorderStyle="Solid">
                    <AlternatingRowStyle BorderColor="#666666" BorderStyle="Solid" BorderWidth="1px" />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid" BorderWidth="1px" />
                    <PagerStyle BorderColor="#666666" BorderStyle="Solid" BorderWidth="1px" />
                    <RowStyle BorderColor="#666666" BorderStyle="Solid" BorderWidth="1px" />
                </asp:GridView>
                </form>
            <hr />
            <br />
        </div>
    </div>
    <div class="mainbottom"></div>
</asp:Content>
