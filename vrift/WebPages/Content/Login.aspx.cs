﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace vrift.WebPages.Content
{
    public partial class Login : System.Web.UI.Page
    {
        string connStr = "server=localhost;user=root;database=vuser;port=3306;password='';SslMode=none;";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.FindControl("login").ID = "current";
        }

        //login button for loggin into an account
        protected void addButton_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                //open connection
                conn.Open();

                string sql = "select uname, pass from member where uname = @un && pass = @ps;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                //add the uname and pass that was inserted into the text feilds
                cmd.Parameters.AddWithValue("@un", uname.Text);
                cmd.Parameters.AddWithValue("@ps", pass.Text);

                //prepare the statement and execute
                cmd.Prepare();
                MySqlDataReader read = cmd.ExecuteReader();

                //if there was no value returned the pass was wrong, throw exception
                while (true)
                {
                    if (!read.Read())
                        throw new Exception("Wrong Username or Pass");
                    else
                    {   //if the sql is valid, login and set the session vars for account and login
                        Session["LoggedIn"] = "logged";
                        Session["acct"] = read[0].ToString();
                        break;
                    }
                }
        

                //then go home
                Response.Redirect("Default.aspx");
            }
            catch (Exception ex)
            {
                //if an error, change the login button text
                loginbutton.Text = "Try again";
            }
            //close our database connection
            conn.Close();
        }

        //signup button for making a new account
        protected void signButton_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                //require cellphone number
                if (phone.Text == "")
                    throw new Exception("Must have Phone Number");



                //connect
                conn.Open();

                //add arguments for sql
                string sql = "insert into phone (number, phonetype) values(@phone, null);";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                //add values to placeholders
                cmd.Parameters.AddWithValue("@phone", phone.Text);

                //add new phone
                cmd.ExecuteNonQuery();

                sql = "insert into member (fname, lname, uname, pass, email, birthdate, phoneid) values (@fn, @ln, @un, @ps, @em, @bd, (select LAST_INSERT_ID()));";
                cmd = new MySqlCommand(sql, conn);

                //add values to placeholders
                cmd.Parameters.AddWithValue("@fn", fname.Text);
                cmd.Parameters.AddWithValue("@ln", lname.Text);
                cmd.Parameters.AddWithValue("@un", uname_s.Text);
                cmd.Parameters.AddWithValue("@ps", pass_s.Text);
                cmd.Parameters.AddWithValue("@em", email.Text);
                cmd.Parameters.AddWithValue("@bd", (birth.Text == "(YYYY-MM-DD)") ? null : birth.Text);
                cmd.Parameters.AddWithValue("@phone", phone.Text);

                //add new phone
                cmd.ExecuteNonQuery();

                //add session uname and login
                Session["LoggedIn"] = "logged";
                Session["acct"] = uname_s.Text;
            }
            catch (Exception ex)
            {
                signupbutton.Text = (ex.ToString() == "Must have Phone Number") ? "Must have Phone Number" : ex.ToString();//"Try again";

            }

            //close our database connection
            conn.Close();

            Response.Redirect("Default.aspx");
        }
    }
}