﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebPages/Master/vrift.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="vrift.WebPages.Content.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <div class="imgholder">
        <img class="headerpic" width="1920" height="1080" src="../../Content/img/random/3.png" />
    </div>
    <div class="picturesbottom"></div>
    <div class="blocker">
        <div class="mainstuff">
            <hr />
            <h2>What is vRift?</h2>
            <hr />
            <p>
                <strong>
                vRift is not only a development and design studio, but so much more! vRift is a project where all of its members function as a team unified under one goal: Innovating the way we create.
                All of us at vRift believe that we should create because it is our passion and we strive together to meet the individual passion of our creators; as well as the team as a whole. We walk
                together to provide the access to resources that may have been inaccessible to the individual, while also promoting creativity. We wish to succeed by thinking out of the box through our
                shared passion of creating.
                </strong>
            </p>
            <hr />
            <br />
        </div>
    </div>
    <div class="mainbottom"></div>
</asp:Content>
